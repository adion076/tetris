"""AI-Ready Tetris Game Layer

Entry point

TODO:
    * Some way to reset/create another game while playing
    * Constants for grid size, currently hardcoded to 10x16
    * More efficient technique to display color, should not need to constantly translate strings

Scripsit Autem Mortale, Leviter Calcare
"""

# pylint: disable=E1101

### IMPORTS ###

import pygame
from pygame.locals import *

from tetris import Tetris

import ai


### CONSTANTS & PARAMETERS ###

# GAME #

STEP_INTERVAL = 700 # in ms

# RENDERING #

# Block size, window size will be determined accordingly
BLOCK_WIDTH = 48 # in pixels
BLOCK_HEIGHT = 48 # in pixels

# Grid
GRID_THICKNESS = 2 # in pixels
GRID_COLOR = (100, 100, 100) # (R, G, B) where R,G and B are ints ranging from 0 to 255

# Background
BACKGROUND_COLOR = (0, 0, 0) # (R, G, B) where R,G and B are ints ranging from 0 to 255


### FUNCTIONS ###

def translate_color(color_str):
    """Translate color string to pygame (R, G, B) format.

    Args:
        color (str): The color's name

    Returns:
        The corresponding (R, G, B) formatted color.

    """
    n_color = color_str.strip().upper() # Normalize the color string
    result = None

    if n_color == "RED":
        result = (255, 0, 0)
    elif n_color == "GREEN":
        result = (0, 255, 0)
    elif n_color == "BLUE":
        result = (0, 0, 255)
    elif n_color == "WHITE":
        result = (255, 255, 255)
    elif n_color == "BLACK":
        result = (0, 0, 0)
    elif n_color == "TEAL":
        result = (0, 128, 128)
    elif n_color == "AQUA":
        result = (0, 255, 255)
    elif n_color == "PURPLE":
        result = (128, 0, 128)
    elif n_color == "YELLOW":
        result = (255, 255, 0)
    elif n_color == "ORANGE":
        result = (255, 165, 0)

    return result


### CLASS ###

class Game:
    """"""

    def __init__(self):
        """"""
        self._running = True
        self._display_surf = None
        self.size = (0, 0)
        self.clock = pygame.time.Clock()

        self.game = None
        self.timer = 0

    def on_init(self):
        """"""
        pygame.init()

        self.game = Tetris() # Create a new game
        self.size = (BLOCK_WIDTH * self.game.width, BLOCK_HEIGHT * self.game.height)

        self._display_surf = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        pygame.display.set_caption("Un pti' Tetris")
        self._running = True

    def on_event(self, event):
        """"""
        if event.type == pygame.QUIT:
            self._running = False
        elif event.type == pygame.KEYDOWN:

            if event.key == pygame.K_LEFT:
                self.game.try_left()

            if event.key == pygame.K_RIGHT:
                self.game.try_right()

            if event.key == pygame.K_UP:
                if self.game.current_piece is not None:
                    self.game.try_rotate_left()

            if event.key == pygame.K_DOWN:
                self.game.step()
                self.timer = 0

            if event.key == pygame.K_SPACE: # Hard drop
                while self.game.current_piece is not None:
                    self.game.step()
                self.game.step() # Instantly spawn new piece
                self.timer = 0

    def update(self, delta):
        """"""
        self.timer += delta

        if self.timer >= STEP_INTERVAL:

            # AI Intervention
            #option = ai.find_best(self.game, 6)

            #if option == "L":
            #    self.game.try_left()
            #elif option == "R":
            #    self.game.try_right()
            #elif option == "Ro":
            #    self.game.try_rotate_left()

            self.game.step()
            self.timer -= STEP_INTERVAL

    def render(self):
        """"""
        # Background
        background = pygame.Surface(self.size)
        background = background.convert()
        background.fill(BACKGROUND_COLOR)
        self._display_surf.blit(background, (0, 0))

        # Game Grid
        grid = pygame.Surface(self.size)
        grid = grid.convert()
        hd_prev = self.game.hard_drop_preview()
        for y_pos in range(0, self.game.height):
            for x_pos in range(0, self.game.width):
                block = pygame.Surface((BLOCK_WIDTH, BLOCK_HEIGHT))
                block = block.convert()

                if hd_prev[x_pos][y_pos] != 0:
                    block.fill(translate_color(hd_prev[x_pos][y_pos]))

                pygame.draw.rect(block, GRID_COLOR, (0, 0, BLOCK_WIDTH, BLOCK_HEIGHT), GRID_THICKNESS)
                grid.blit(block, (x_pos * BLOCK_WIDTH, y_pos * BLOCK_HEIGHT))

        # Current Piece
        if self.game.current_piece is not None:
            for pos in self.game.current_piece.shape:
                block = pygame.Surface((BLOCK_WIDTH, BLOCK_HEIGHT))
                block = block.convert()
                block.fill(translate_color(self.game.current_piece.color))

                grid.blit(block, ((pos[0] + self.game.current_piece.x_pos) * BLOCK_WIDTH,
                                  (pos[1] + self.game.current_piece.y_pos) * BLOCK_HEIGHT))

        self._display_surf.blit(grid, (0, 0))

        # Current Score
        font = pygame.font.Font(None, 36)
        text = font.render("{0:d}pts".format(self.game.score), 1, (255, 255, 255))
        textpos = text.get_rect()
        textpos.centerx = background.get_rect().centerx
        self._display_surf.blit(text, (0, 0))

        pygame.display.update()

    def on_cleanup(self):
        """"""
        pygame.quit()

    def on_execute(self):
        """"""
        if self.on_init():
            self._running = False

        self.clock.tick() # Discard the initial clock tick
        while self._running:
            for event in pygame.event.get():
                self.on_event(event)

            if not self.game.done:
                # Compute delta time and update game
                delta = self.clock.tick() # In ms
                self.update(delta)

            self.render()

        self.on_cleanup()


### SCRIPT ###

def main():
    """"""
    game = Game()
    game.on_execute() # Launch the game


if __name__ == "__main__":
    main()
