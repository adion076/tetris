"""

# TODO: Make sure the random states are handled correctly
# TODO: Should we try all the possible rotations?
# TODO: Make sure to use try_rotate_left when it is completed
# TODO: Sparsity ratio heuristic?

Scripsit Autem Mortale, Leviter Calcare
"""

### IMPORTS ###

import copy
import random
import time

from tetris import Tetris


### FUNCTIONS ###

# Utility Functions

def cleared_lines(grid):
    """"""

    line_count = 0
    for y_pos in range(len(grid[0])):
        count_blocks = 0
        for x_pos in range(len(grid)):
            if grid[x_pos][y_pos] != 0:
                count_blocks += 1

        if count_blocks == len(grid): # Full line
            line_count += 1

    # Assign points
    score = 0
    if line_count == 1:
        score += 100 # Single
    elif line_count == 2:
        score += 300 # Double
    elif line_count == 3:
        score += 500 # Triple
    elif line_count == 4:
        score += 800 # Tetris
    elif line_count > 4:
        print("Something went wrong. Found {0} completed lines...".format(line_count))

    return score


# Heuristic Functions

def heur_estimate(tetris):
    """"""
    hard_drop = tetris.hard_drop_preview()
    top_block = highest_block(tetris.grid) # Find the highest block, used in a few costs
    cleared_pts = cleared_lines(hard_drop)

    score = tetris.score + cleared_pts
    score += holes(hard_drop) * -50
    score += (tetris.height - top_block) * -10
    score += -grid_state(hard_drop)
    #score += ratio(tetris, top_block) * 100
    #score += tallest_empty_column(tetris) * 700

    if tetris.current_piece is not None:
        score += tetris.current_piece.y_pos * 1

    if tetris.done:
        score -= 2000

    return score

def grid_state(grid):
    """Give a score to the current grid according to the depth of the blocks."""

    score = 0

    for y_pos in range(len(grid[0])):
        for x_pos in range(len(grid)):
            if grid[x_pos][y_pos] != 0:
                score += len(grid[0]) - y_pos

    return score

def ratio(tetris, top_block):
    """"""

    if top_block == tetris.height:
        return 2

    count = 0

    for y_pos in range(tetris.height):
        for x_pos in range(tetris.width):
                if tetris.grid[x_pos][y_pos] != 0:
                    count += 1

    return count / ((tetris.height - top_block) * tetris.width)

# TODO: Test this
def tallest_empty_column(grid):
    """"""
    tallest = 0
    for x_pos in range(len(grid)):
        count = 0
        for y_pos in range(len(grid[0])):
            if grid[x_pos][y_pos] == 0:
                count += 1
            else:
                break

        if count > tallest:
            tallest = count

    return tallest


def highest_block(grid):
    """Get the y height of the highest block."""

    for y_pos in range(len(grid[0])):
        for x_pos in range(len(grid)):
            if grid[x_pos][y_pos] != 0:
                return y_pos

    return len(grid[0]) # The grid is empty

# TODO: Test this
def holes(grid):
    """Count the amount of blocks with a filled block above."""

    count = 0

    for y_pos in range(len(grid[0]) - 1):
        for x_pos in range(len(grid)):
            if grid[x_pos][y_pos + 1] == 0:
                if grid[x_pos][y_pos] != 0:
                    count += 1

    return count

# Modified A*

def find_best(tetris, depth=1, top=True):
    """"""

    if depth == 0:
        return heur_estimate(tetris)

#    if top and tetris.current_piece is not None and skip(tetris, depth):
#        return 'N'

    if top:
        start_time = time.perf_counter() # Performance measurements

    # 4 actions are possible, left, right, rotate, nothing, let's recursively explore them all

    # Nothing
    tetris_nothing = copy.deepcopy(tetris)
    tetris_nothing.step()
    nothing_score = find_best(tetris_nothing, depth - 1, False)

    if tetris.current_piece is not None:
        # Left
        tetris_left = fast_copy(tetris_nothing, tetris)
        tetris_left.try_left()
        tetris_left.step()
        left_score = find_best(tetris_left, depth - 1, False)

        # Right
        tetris_right = fast_copy(tetris_left, tetris)
        tetris_right.try_right()
        tetris_right.step()
        right_score = find_best(tetris_right, depth - 1, False)

        # Rotate
        tetris_rotate = fast_copy(tetris_right, tetris)
        tetris_rotate.try_rotate_left()
        tetris_rotate.step()
        rotate_score = find_best(tetris_rotate, depth - 1, False)

        #print("[d={0}] N:{1:7d} | L:{2:7d} | R:{3:7d} | Ro: {4:7d}".format(depth, nothing_score, left_score, right_score, rotate_score))
    else:
        #print("[d={0}] N:{1:7d}".format(depth, nothing_score)) # No piece currently controlled by the player.
        return nothing_score

    if top:

        stop_time = time.perf_counter()
        perf_time = int((stop_time - start_time) * 1000)
        print("Depth {0} took {1}ms".format(depth, perf_time))
        end_time = time.perf_counter()

        options = {'N':nothing_score, 'L':left_score, 'R':right_score, 'Ro':rotate_score}

        max_score = max(options.values())
        best_opts = [key for key,val in options.items() if val == max_score]
        random.shuffle(best_opts)

        return best_opts[0]

    return max(nothing_score, left_score, right_score, rotate_score)

def skip(tetris, depth):
    """Detect whether or not to select the nothing option right away."""
    for y_pos in range(depth + 2):
        y_pos_act = y_pos + tetris.current_piece.y_pos

        if y_pos_act >= tetris.height:
            return False # Outside of the grid

        for x_pos in range(tetris.width):
            if tetris.grid[x_pos][y_pos_act] != 0:
                return False

    return True


def fast_copy(old_tetris, new_tetris):
    """Faster copy so we don't have to deep copy the grid everytime."""

    # Copy the new grid in place of old grid
    for x_pos in range(old_tetris.width):
        for y_pos in range(old_tetris.height):
            old_tetris.grid[x_pos][y_pos] = new_tetris.grid[x_pos][y_pos]

    # Copy the game piece
    old_tetris.current_piece = copy.deepcopy(new_tetris.current_piece)

    # Copy other relevant data
    old_tetris.score = new_tetris.score
    old_tetris.done = new_tetris.done
    old_tetris.last_tetris = new_tetris.last_tetris

    return old_tetris
