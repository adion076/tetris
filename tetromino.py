"""AI-Ready Tetris Tetromino Piece

TODO:
    * Cleanup the rotation functions

Scripsit Autem Mortale, Leviter Calcare
"""

### IMPORTS ###

import random


### CLASS ###

class Tetromino():
    """"""

    def __init__(self, initial_x=0, initial_y=0):
        """Constructor"""

        self.x_pos = initial_x
        self.y_pos = initial_y

        self.shape = [] # Where the first element is the center of rotation
        self.color = None

        self.no_rotate = False # Stop the block form rotating, used for square shape

    def random_shape(self):
        """Assign a random shape and its corresponding color to the Tetromino."""

        rand_int = random.randint(0, 6)

        if rand_int == 0:
            self.shape = [[1, 0], [0, 0], [2, 0], [1, 1]] # T Shape
            self.color = "Purple"
        elif rand_int == 1:
            self.shape = [[1, 0], [0, 0], [2, 0], [3, 0]] # I Shape
            self.color = "Aqua"
        elif rand_int == 2:
            self.shape = [[0, 0], [1, 1], [0, 1], [1, 0]] # Square Shape
            self.color = "Yellow"
            self.no_rotate = True
        elif rand_int == 3:
            self.shape = [[1, 1], [0, 0], [1, 0], [2, 1]] # S-shape
            self.color = "Green"
        elif rand_int == 4:
            self.shape = [[1, 1], [0, 1], [1, 0], [2, 0]] # Z-Shape
            self.color = "Red"
        elif rand_int == 5:
            self.shape = [[1, 1], [0, 1], [2, 1], [2, 0]] # L Shape
            self.color = "Orange"
        elif rand_int == 6:
            self.shape = [[1, 0], [0, 0], [2, 0], [2, 1]] # J Shape
            self.color = "Blue"
        else:
            print("Tried to generate unknown shape {0}".format(rand_int))

    def rotate_left(self):
        """Rotate the Tetromino counter-clockwise.

        Accomplished by first translating the piece further into the first quadrant, rotating to
            the fourth quadrant, and translating the piece back to the origin.

        """
        if self.no_rotate:
            return

        new_shape = []
        center = self.shape[0]
        for i in range(len(self.shape)):
            block = self.shape[i]
            if i != 0: # Don't rotate the center of rotation
                adj_pos = [block[0] - center[0] + 10, block[1] - center[1] + 10] # Adjusted position to "rotate"
                new_pos = [0, 0]

                new_pos[0] = -adj_pos[1] + center[0] + 10
                new_pos[1] = adj_pos[0] + center[1] - 10

                new_shape.append(new_pos)

            else:
                new_shape.append(block)

        self.shape = new_shape

    def rotate_right(self):
        """Rotate the Tetromino clockwise.

        Accomplished by first translating the piece further into the first quadrant, rotating to
            the second quadrant, and translating the piece back to the origin.

        """
        if self.no_rotate:
            return

        new_shape = []
        center = self.shape[0]
        for i in range(len(self.shape)):
            block = self.shape[i]
            if i != 0: # Don't rotate the center of rotation
                adj_pos = [block[0] - center[0] + 10, block[1] - center[1] + 10] # Adjusted position to "rotate"
                new_pos = [0, 0]

                new_pos[0] = adj_pos[1] + center[0] - 10
                new_pos[1] = -adj_pos[0] + center[1] + 10

                new_shape.append(new_pos)

            else:
                new_shape.append(block)

        self.shape = new_shape

    def move_up(self):
        """Move the piece up one square."""

        self.y_pos -= 1

    def move_down(self):
        """Move the piece down one square."""

        self.y_pos += 1

    def move_left(self):
        """Move the piece left one square."""

        self.x_pos -= 1

    def move_right(self):
        """Move the piece right one square."""

        self.x_pos += 1

    def get_occupied_positions(self):
        """Return a list of occupied grid poisitions."""

        result = []

        for block in self.shape:
            result.append([block[0] + self.x_pos, block[1] + self.y_pos])

        return result

    def print_shape(self):
        """DEBUG"""

        for y_pos in range(-3, 3):
            for x_pos in range(-4, 4):
                if [x_pos, y_pos] in self.shape:
                    print("█", end='')
                else:
                    print(" ", end='')
            print("") # New line
