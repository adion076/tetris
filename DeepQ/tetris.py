"""AI-Ready Tetris

TODO:
    * Improve code so that we don't need to transpose the whole grid to remove a line
        Might want to use a timer to measure performance (saving as much ms as possible is gonna important later on)

Scripsit Autem Mortale, Leviter Calcare
"""

### IMPORTS ###

import copy
import subprocess
import time

from tetromino import Tetromino


### CLASSES ###

class Tetris():
    """"""

    def __init__(self, width=10, height=16):
        """Constructor"""

        self.height = height
        self.width = width
        self.grid = [[0 for i in range(height)] for j in range(width)] # Where 0,0 is the top left corner
        self.score = 0
        self.done = False

        self.last_tetris = False # Whether or not the last line clear was a Tetris
        self.current_piece = None

    def new_piece(self):
        """Generate a random piece and place it in the current piece slot."""

        self.current_piece = Tetromino(4, 0)
        self.current_piece.random_shape()

    def update_grid(self):
        """"""
        line_count = 0
        for y_pos in range(len(self.grid[0])):
            count_blocks = 0
            for x_pos in range(len(self.grid)):
                if self.grid[x_pos][y_pos] is not None and self.grid[x_pos][y_pos] != 0:
                    count_blocks += 1

            # Clear the completed line
            # TODO: Might require some performance improvements
            if count_blocks == len(self.grid): # Full line
                t_grid = [list(i) for i in zip(*self.grid)] # Transpose the grid, makes it easier to pop the line
                line_count += 1
                t_grid.pop(y_pos)
                t_grid.insert(0, [0] * self.width) # Insert a new empty line

                self.grid = [list(i) for i in zip(*t_grid)]

        # Assign points
        if line_count == 1:
            self.score += 100 # Single
            self.last_tetris = False
        elif line_count == 2:
            self.score += 300 # Double
            self.last_tetris = False
        elif line_count == 3:
            self.score += 500 # Triple
            self.last_tetris = False
        elif line_count == 4:
            if self.last_tetris:
                self.score += 1200 # Back-to-back Tetris
            else:
                self.score += 800 # Tetris
                self.last_tetris = True
        elif line_count > 4:
            print("Something went wrong. Found {0} completed lines...".format(line_count))

    def try_left(self):
        """"""
        if self.current_piece is not None:
            self.current_piece.move_left()

            if not self.check_empty(self.current_piece.get_occupied_positions()):
                self.current_piece.move_right() # Move back

    def try_right(self):
        """"""
        if self.current_piece is not None:
            self.current_piece.move_right()

            if not self.check_empty(self.current_piece.get_occupied_positions()):
                self.current_piece.move_left() # Move back

    def try_rotate_left(self):
        """"""
        if self.current_piece is not None:
            self.current_piece.rotate_left()

            if not self.check_empty(self.current_piece.get_occupied_positions()):
                self.current_piece.rotate_right() # Move back

    def try_rotate_right(self):
        """"""
        if self.current_piece is not None:
            self.current_piece.rotate_right()

            if not self.check_empty(self.current_piece.get_occupied_positions()):
                self.current_piece.rotate_left() # Move back

    def step(self):
        """Move the current piece down, if possible."""

        if self.current_piece is None:
            self.new_piece()
            possible = self.check_empty(self.current_piece.get_occupied_positions())

            if not possible:
                self.done = True # The game is done
                return


        self.current_piece.move_down()
        possible = self.check_empty(self.current_piece.get_occupied_positions())

        if not possible:
            self.current_piece.move_up() # Move the piece back

            blocks = self.current_piece.get_occupied_positions()
            for pos in blocks:
                self.grid[pos[0]][pos[1]] = self.current_piece.color # Fill-in the grid

            self.current_piece = None # Destroy the piece, it now lives in the grid
            self.update_grid() # Check if any new lines were completed


    def hard_drop_lc(self,grid):

        new_grid = grid
        clear_score = 0
        line_count = 0
        for y_pos in range(len(grid[0])):
            count_blocks = 0
            for x_pos in range(len(grid)):
                if grid[x_pos][y_pos] is not None and grid[x_pos][y_pos] != 0:
                    count_blocks += 1

            # Clear the completed line
            # TODO: Might require some performance improvements
            if count_blocks == len(grid): # Full line
                t_grid = [list(i) for i in zip(*grid)] # Transpose the grid, makes it easier to pop the line
                line_count += 1
                t_grid.pop(y_pos)
                t_grid.insert(0, [0] * len(grid)) # Insert a new empty line

                new_grid = [list(i) for i in zip(*t_grid)]

        # Assign points
        if line_count == 1:
            clear_score += 100 # Single
        elif line_count == 2:
            clear_score += 300 # Double
        elif line_count == 3:
            clear_score += 500 # Triple
        elif line_count == 4:
            clear_score += 800 # Tetris
        elif line_count > 4:
            print("Something went wrong. Found {0} completed lines...".format(line_count))
        
        return new_grid, clear_score

    def hard_drop_preview(self):
        """"""

        if self.current_piece is None:
            return None,0 # We don't have piece to preview

        org_x_pos = self.current_piece.x_pos # Keep the previous position of the piece  of the grid so we can restore it
        org_y_pos = self.current_piece.y_pos
        while 1 < 2:
            self.current_piece.move_down()
            possible = self.check_empty(self.current_piece.get_occupied_positions())

            if not possible:
                self.current_piece.move_up() # Move the piece back
                break # This is the lowest we can go, exit the loop

        # Fill in the grid preview with the preview piece
        grid_preview = copy.deepcopy(self.grid)
        for pos in self.current_piece.get_occupied_positions():
            grid_preview[pos[0]][pos[1]] = "White"

        grid_preview_lc, score = self.hard_drop_lc(grid_preview)

        #Might create problems with line clear
        self.current_piece.x_pos = org_x_pos # Restore the original block position
        self.current_piece.y_pos = org_y_pos

        return grid_preview_lc, score

   
        

    def check_empty(self, abs_shape):
        """Check if the spot is emtpy, return True if it is."""

        empty = True
        for pos in abs_shape:
            if pos[1] == self.height: # Invalid Y position
                empty = False
                break # Parts of the shape is out of bounds
            elif pos[0] > (self.width - 1) or pos[0] < 0: # Invalid X position
                empty = False
                break # Parts of the shape is out of bounds
            elif self.grid[pos[0]][pos[1]] != 0:
                empty = False
                break

        return empty

    def save_current_piece(self):
        """Save current piece to grid"""
        if self.current_piece is not None:
            for pos in self.current_piece.get_occupied_positions():
               self.grid[pos[0]][pos[1]] = "White"
        
    def print_grid(self):
        """DEBUG"""

        for y_pos in range(0, self.height):
            for x_pos in range(0, self.width):
                if self.current_piece is not None and [x_pos, y_pos] in self.current_piece.get_occupied_positions():
                    print("▓ ", end='')
                elif self.grid[x_pos][y_pos] != 0:
                    print("█ ", end='')
                else:
                    print("░ ", end='')
            print("") # New line

        if self.current_piece is not None:
            print("Current piece is {0} at [{1},{2}]".format(self.current_piece.shape, self.current_piece.x_pos, self.current_piece.y_pos))
        else:
            print("No shape on board...")


### SCRIPT ###

def main():
    """"""
    game = Tetris()

    phase = False
    while 1 != 2:
        if game.current_piece is not None and phase:
            game.current_piece.rotate_left()
            phase = False
        else:
            phase = True
            game.step()
            game.hard_drop_preview()

        game.print_grid()
        time.sleep(1)
        subprocess.call('clear', shell=True)



if __name__ == "__main__":
    main()
