### IMPORTS ###
import random
import numpy as np
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam


### CLASS ###
class DeepQ():
    """"""
    #Deep defintion 
    def __init__(self):
        self.action_size = 4
        self.state_size = 1
        self.memory = deque(maxlen=20000)
        self.gamma = 0.95    # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0
        self.epsilon_decay = 0.995
        self.learning_rate = 0.001
        self.replay_size = 200
        self.model = self._build_model()

    def _build_model(self):
        # Neural Net for Deep-Q learning Model
        model = Sequential()
        model.add(Dense(32, input_dim=self.state_size, activation='relu'))
        model.add(Dense(32, activation='relu'))
        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss='mse',optimizer=Adam(lr=self.learning_rate))
        return model

    #Q-Learning
    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def chooseAction(self, state):
    
        rand = np.random.rand()
        if rand <= self.epsilon:
            return random.randrange(self.action_size)
        prediction_grid = self.gridFlatten(state.grid)
        act_values = self.model.predict(prediction_grid)
        best_pred = np.unravel_index(act_values.argmax(), act_values.shape)
        return np.argmax(act_values[best_pred[0]])  # retourne la meilleure prediction d'action

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
                target = (reward + self.gamma*np.amax(self.model.predict(next_state)))
            target_f = self.model.predict(state)
            target_f[0][action] = target
            self.model.fit(state, target_f, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
    
    ### Rewards ###
    def hole_penalty(self, grid):
        """Define the reward/penalty for leaving holes"""
        hole_score=0
        for x in range(len(grid)):
            for y in range(len(grid[0])):
                if grid[x][y] is not None and grid[x][y] != 0:
                #Found block, check for holes below; check if reached the bottom
                    hole_depth=1
                    index=0
                    while y+index<= len(grid[0])-1:
                        if grid[x][y+index] is None or grid[x][y+index] == 0:
                            #Count hole_depth and add to hole_score
                            hole_score-=hole_depth
                            hole_depth+=1                          
                        index+=1
                    break
        return hole_score

    def average_height(self, grid):
        """Give reward for large average height"""
        height_score=0
        for y in range(len(grid[0])):
            for x in range(len(grid)):
                if grid[x][y] is not None and grid[x][y] != 0:
                    height_score+= len(grid[0])-y
                    pass
        return height_score

    def last_column_empty(self, grid):
        """Give reward for leaving one empty column (assume right side)"""
        empty_column=True
        #Select bonus Weight
        column_bonus=10
        for y in range(len(grid[0])):
            if grid[len(grid)-1][y] is not None and grid[len(grid)-1][y] != 0:
                empty_column=False
        if empty_column==True:
            column_score=column_bonus
        else:
            column_score=-column_bonus
        return column_score

    def bumps(self, grid):
        column1=-1
        column2=-1
        bump_score=0
        for x in range(len(grid)):
            for y in range(len(grid[0])):
               if grid[x][y] is not None and grid[x][y] != 0:
                   if column1==-1:
                       column1=y
                       break
                   elif column2==-1:
                       column2=y
                       bump_score+=abs(column2-column1)
                       break
                   elif x%2==0:
                       column1=y
                       bump_score+=abs(column1-column2)
                       break
                   else:
                       column2=y
                       bump_score+=abs(column2-column1)
                       break        
        return bump_score

    
    def current_block_height(self,Tetris):
        if Tetris.current_piece is not None:
            return Tetris.current_piece.y_pos
        else:
            return 0
    
    def immediate_reward(self,Tetris):
        
        dropPreviewGrid, clear_score = Tetris.hard_drop_preview()
        if (dropPreviewGrid!=None):
            grid = dropPreviewGrid
        else:
            grid = Tetris.grid
        hr = self.hole_penalty(grid)
        ahr = self.average_height(grid)
        ecr = self.last_column_empty(grid)
        bs = self.bumps(grid)
        cbh = self.current_block_height(Tetris)
        #ImReward = (100*hr) -(ahr//2) + ecr + (clear_score**2) - (bs*500) + cbh
        ImReward = (10*hr) -(ahr//5) + ecr + (clear_score**2) - (bs*2) + (cbh*2)
        return ImReward

    #Flatten for Deep
    def gridFlatten(self,grid):
        newGrid=[]
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                if grid[i][j] is not None and grid[i][j]!=0:
                    newGrid.append(1)
                else:
                    newGrid.append(0)
        return newGrid;

